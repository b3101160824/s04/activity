package com.zuitt.wdc044_s01.services;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    void createPost(String stringToken, Post post);
//    Now that we are generating JWT, ownership of a blog post will be retreive from the JWT's payload
    Iterable<Post> getPosts();

//                           post id   , user Credentials, Request Body
    ResponseEntity updatePost(Long id, String stringToken, Post post);
//
    ResponseEntity deletePost(Long id, String stringToken);
}
