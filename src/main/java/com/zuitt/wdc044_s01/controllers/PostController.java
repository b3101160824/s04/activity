package com.zuitt.wdc044_s01.controllers;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.repositories.PostRepository;
import com.zuitt.wdc044_s01.services.PostServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import com.zuitt.wdc044_s01.services.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController // Handle HTTP Request and generating appropriate Responses
// To Enable Cross origin resource sharing
// Example: When a client (browser) makes a request to a server, the server typically restricts access to its resource originating from the other origin
@CrossOrigin // 4.8 - Render Host API
public class PostController {

    @Autowired
    static PostService postService;

//   Create a new Post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post) {
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }
    /* @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        Iterable<Post> posts = postService.getPosts();
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }*/

    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts() {
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }
}
